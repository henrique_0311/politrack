from typing import ContextManager
from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.forms import UserCreationForm
from .models import *
from .forms import *
from django.forms.widgets import HiddenInput
from .funcAux import contexto_total
from django.db.models import Sum


# Create your views here.

@user_passes_test(lambda User: not(User.is_authenticated),login_url="AppPolitrack:home")
def loginPage(request):
    
    context = {'pagina':'login'}

    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        print("\nUsuario: ",username)
        print("Senha: ",password)

        user = authenticate(request,username = username,password = password)
        if user is not None:
            login(request,user)
            return redirect("AppPolitrack:home")
        else:
            context['ErrorMessage'] = "Usuário ou senha estão incorretos!"
        
    
    return render(request,"AppPolitrack/Paginas/login_register.html",context)

def logoutPage(request):
    logout(request)
    return redirect("AppPolitrack:home")

def home(request):
    context = contexto_total(request)
    return render(request,"AppPolitrack/Paginas/home.html",context)







#
# --------------------Casos DE Uso
#

#Cadastro Gerente ---View Temporaria
def cdr0(request): 
    form = Form_Cria_PrestadorDeServiço(initial={'tipo': 'Gerente'})

    if request.method =="POST":
        form = Form_Cria_PrestadorDeServiço(request.POST)
        
        if form.is_valid():

            form.save()
            
            user = authenticate(
                request,
                username = form.cleaned_data['username'],
                password = form.cleaned_data['password1'],
            )
            login(request,user)
            return redirect("AppPolitrack:home")
    context = {}
    context['form'] = form

    return render(request,"AppPolitrack/Paginas/cdr0.html",context)

# Cadastro de prestador de serviço
def cdr1(request): 
    form = Form_Cria_PrestadorDeServiço(initial={'tipo': 'Prestador de Serviço'})

    if request.method =="POST":
        form = Form_Cria_PrestadorDeServiço(request.POST)
        
        if form.is_valid():

            form.save()
            
            user = authenticate(
                request,
                username = form.cleaned_data['username'],
                password = form.cleaned_data['password1'],
            )
            login(request,user)
            return redirect("AppPolitrack:home")
    context = {}
    context['form'] = form
    return render(request,"AppPolitrack/Paginas/cdr1.html",context)

#Criação de tipo de serviço
@login_required(login_url="AppPolitrack:home")
@user_passes_test(lambda User: User.tipo == "Gerente",login_url="AppPolitrack:home")
def cdr2(request): 
    form = Form_TipoServiço
    if request.method =="POST":
        form = Form_TipoServiço(request.POST)
        if form.is_valid():
            form.save()
            return redirect("AppPolitrack:home")
    context = contexto_total(request)
    context['form'] = form  
    return render(request,"AppPolitrack/Paginas/cdr2.html",context)

# cadastro de cliente
def cdr3(request): 

    form = Form_Cria_Cliente(initial={'tipo': 'Cliente'})

    if request.method =="POST":
        form = Form_Cria_Cliente(request.POST)
        
        if form.is_valid():
            form.save()
            user = authenticate(
                request,
                username = form.cleaned_data['username'],
                password = form.cleaned_data['password1'],
            )
            login(request,user)
            return redirect("AppPolitrack:home")
    context = {}
    context['form'] = form
    return render(request,"AppPolitrack/Paginas/cdr3.html",context)
    
 
#Cadastro de inventário
@login_required(login_url="AppPolitrack:home")
@user_passes_test(
    lambda User: User.tipo in ["Gerente","Prestador de Serviço"],
    login_url="AppPolitrack:home")
def cdr4(request):
    form = Form_Inventario
    if request.method =="POST":
        form = Form_Inventario(request.POST)
        if form.is_valid():
            form.save()
            return redirect("AppPolitrack:home")
    context = contexto_total(request)
    context['form'] = form   
    return render(request,"AppPolitrack/Paginas/cdr4.html",context)


#Solicitação de orçamento para serviço
@login_required(login_url="AppPolitrack:home")
@user_passes_test(
    lambda User: User.tipo in ["Cliente"],
    login_url="AppPolitrack:home")
def orç1(request):
    form =Form_Serviço_1(
        initial={
            'cliente': Usuario.objects.get(id = request.user.id),
            'status': "Em processamento",
        }
    )
    if request.method =="POST":
        form = Form_Serviço_1(request.POST)
        if form.is_valid():
            form.save()
            return redirect("AppPolitrack:home")

    context = contexto_total(request)
    context['form'] = form
    return render(request,"AppPolitrack/Paginas/orç1.html",context)



@login_required(login_url="AppPolitrack:home")
@user_passes_test(
    lambda User: User.tipo in ["Gerente","Cliente"],
    login_url="AppPolitrack:home")
def orç2_1(request):
    context = contexto_total(request)
    return render(request,"AppPolitrack/Paginas/orç2_1.html",context)

def orç2_2(request,pk):
    
    Solicitação_de_orçamento = tbl_Serviço.objects.get(id = pk)

    form =Form_Serviço_2(
        initial={
            'cliente': Usuario.objects.get(id = request.user.id),
            'status': "Solicitação Avaliada",
        },
        instance = Solicitação_de_orçamento
    )
    if request.method =="POST":
        form = Form_Serviço_2(request.POST,instance = Solicitação_de_orçamento)
        if form.is_valid():
            form.save()
            return redirect("AppPolitrack:home")
    
    context = contexto_total(request)
    context['Solicitação_de_orçamento'] = Solicitação_de_orçamento
    context['form'] = form
    
    return render(request,"AppPolitrack/Paginas/orç2_2.html",context) 

def os1_1(request):
    context = contexto_total(request)
    return render(request,"AppPolitrack/Paginas/os1_1.html",context)

def os1_2(request,pk,ação):
    serviço = tbl_Serviço.objects.get(id = pk)
    if ação == 'Confirmar':
        serviço.status = "Em execução"
    elif ação == 'Cancelar':
        serviço.status = "Cancelado "
    elif ação == 'Finalizar':
        serviço.status = "Finalizado"
        serviço.save()
        return redirect("AppPolitrack:exibe_serv_em_andamento")
    serviço.save()
    return redirect("AppPolitrack:os1_1")


def exibe_serv_em_andamento(request):
        
    context = contexto_total(request)
    print('testes----')
    #{serviço_id:{Horas_totais:x,horas_prestador:y}}
    horas_gastas = {}

    for serviço in tbl_Serviço.objects.filter(status='Em execução'):
        horas_gastas[serviço.id] = {
            'Horas_totais'    :float(tbl_Serviço_PrestadorServiço.objects.filter(serviço = serviço ).aggregate(Sum('horas_gastas'))['horas_gastas__sum'] or 0),
            'Horas_prestador' :float(tbl_Serviço_PrestadorServiço.objects.filter(serviço = serviço,prestador_de_serviço=request.user ).aggregate(Sum('horas_gastas'))['horas_gastas__sum'] or 0) 
        }

    context['horas_gastas'] = horas_gastas
    return render(request,"AppPolitrack/Paginas/exibe_serv_em_andamento.html",context)

def exibe_Inventario(request):        
    context = contexto_total(request)
    return render(request,"AppPolitrack/Paginas/exibe_Inventario.html",context)

def lan1(request,pk):
    serviço = tbl_Serviço.objects.get(id = pk)

    form =Form_Lan1(
        initial={
            'serviço': serviço,
        },
    )
    if request.method =="POST":
        form = Form_Lan1(request.POST)
        if form.is_valid():
            nova_transação = form.save()
            nova_transação.valor = -abs(nova_transação.valor)
            serviço.valor_despesas += -1 * nova_transação.valor

            nova_transação.save()
            serviço.save()
            return redirect("AppPolitrack:exibe_serv_em_andamento")
    
    context = contexto_total(request)
    context['form'] = form
    context['nome_serviço'] = serviço.nome
    return render(request,"AppPolitrack/Paginas/lan1.html",context) 
    


# Não implementados
def lan2(request,pk):
    serviço = tbl_Serviço.objects.get(id = pk)

    form =Form_Lan2(
        initial={
            'serviço': serviço,
        },
    )
    if request.method =="POST":
        form = Form_Lan2(request.POST)
        if form.is_valid():
            nova_transação = form.save()
            nova_transação.valor = abs(nova_transação.valor)
            serviço.valor_receitas +=  nova_transação.valor

            nova_transação.save()
            serviço.save()
            return redirect("AppPolitrack:exibe_serv_em_andamento")
    
    context = contexto_total(request)
    context['form'] = form
    context['nome_serviço'] = serviço.nome
    return render(request,"AppPolitrack/Paginas/lan2.html",context) 

def est1(request,pk):
    serviço = tbl_Serviço.objects.get(id = pk)

    form =Form_Est1(
        initial={
            'serviço': serviço,
        },
    )
    if request.method =="POST":
        form = Form_Est1(request.POST)
        if form.is_valid():
            nova_transação = form.save()
            #tratamento formulário
            nova_transação.variação_item_inventário = -abs(nova_transação.variação_item_inventário)

            item_inventario = tbl_Inventario.objects.get(id=form.data['item_inventário'])
            item_valor_unitario = item_inventario.valor_total / item_inventario.quantidade

            
            

            serviço.valor_despesas += item_valor_unitario * abs(nova_transação.variação_item_inventário)
            item_inventario.valor_total -= item_valor_unitario * abs(nova_transação.variação_item_inventário)
            item_inventario.quantidade += nova_transação.variação_item_inventário



            serviço.save()
            nova_transação.save()
            item_inventario.save() 
            return redirect("AppPolitrack:exibe_serv_em_andamento")
    
    context = contexto_total(request)
    context['form'] = form
    context['nome_serviço'] = serviço.nome
    return render(request,"AppPolitrack/Paginas/est1.html",context) 

def est2(request):

    form =Form_Est2()
    if request.method =="POST":
        form = Form_Est2(request.POST)
        if form.is_valid():
            nova_transação = form.save()
            nova_transação.variação_item_inventário = abs(nova_transação.variação_item_inventário)
            nova_transação.valor = -abs(nova_transação.valor)
            nova_transação.save()

            item_inventario = tbl_Inventario.objects.get(id=form.data['item_inventário'])

            item_inventario.quantidade += nova_transação.variação_item_inventário
            item_inventario.valor_total +=  -1 * nova_transação.valor
            item_inventario.save() 
            return redirect("AppPolitrack:home")
    
    context = contexto_total(request)
    context['form'] = form

    return render(request,"AppPolitrack/Paginas/est2.html",context) 

def inclusão_horas(request,pk):




    form =Form_inclusão_horas(
        initial={
            'serviço': tbl_Serviço.objects.get(id=pk),
            'prestador_de_serviço': request.user
        },
    )

    if request.method =="POST":
        form = Form_inclusão_horas(request.POST)
        if form.is_valid():
            form.save()
            return redirect("AppPolitrack:exibe_serv_em_andamento")
    context = contexto_total(request)
    context['form'] = form

    return render(request,"AppPolitrack/Paginas/inclusão_horas.html",context) 





def que1(request):
    pass