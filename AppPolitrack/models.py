from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractUser
from django.db.models import Sum
from django import forms
import datetime



#--------------------Usuários-----------------------------------------

class Usuario(AbstractUser):
  
    CPF_ou_CNPJ = models.CharField(max_length=20)
    data_atualização_registro = models.DateTimeField(auto_now=True)
    data_criação_registro = models.DateTimeField(auto_now_add=True)
    tipo = models.CharField(max_length=20)    





# Create your models here.
class tbl_TipoServiço(models.Model):
    nome = models.CharField(max_length=200)
    descrição = models.TextField(null=True, blank=True)
    data_atualização_registro = models.DateTimeField(auto_now=True)
    data_criação_registro = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.nome

class tbl_Inventario(models.Model):
    nome = models.CharField(max_length=200)
    descrição = models.TextField(null=True, blank=True)
    quantidade = models.IntegerField(default=0)
    valor_total = models.DecimalField(max_digits=12,decimal_places=2,default=0)
    quantidade_minima = models.IntegerField(default=0)
    data_atualização_registro = models.DateTimeField(auto_now=True)
    data_criação_registro = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.nome

class tbl_Serviço(models.Model):
    
    #id_cliente
    cliente = models.ForeignKey(
        Usuario,on_delete=models.CASCADE
    )
    #id_tipoServiço
    tipo_serviço = models.ForeignKey(
        tbl_TipoServiço,
        on_delete=models.CASCADE
    )
    status = models.CharField(max_length=40) 
    valor = models.DecimalField(max_digits=12,decimal_places=2,default=0)
    descrição = models.TextField()
    nome = models.CharField(max_length=200)
    valor_despesas = models.DecimalField(max_digits=12,decimal_places=2,default=0,null=True)
    valor_receitas = models.DecimalField(max_digits=12,decimal_places=2,default=0,null=True)

    data_limite = models.DateField()
    data_preferencial = models.DateField(null=True, blank=True)
    
    data_validade_orçamento = models.DateField(null=True,blank=True)
    data_prevista = models.DateField(null=True,blank=True)
    data_inicio_efetivo = models.DateField(null=True, blank=True)
    data_conclusão_efetiva = models.DateField(null=True, blank=True)


    data_atualização_registro = models.DateTimeField(auto_now=True)
    data_criação_registro = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return self.nome

class tbl_Serviço_PrestadorServiço(models.Model):
    
    serviço = models.ForeignKey(
        tbl_Serviço,
        on_delete=models.SET_NULL,
        null=True, blank=True
    )

    prestador_de_serviço = models.ForeignKey(
        Usuario,
        on_delete=models.SET_NULL,
        null=True, blank=True
    )
    data = models.DateField(default=datetime.date.today)
    horas_gastas = models.DecimalField(max_digits=12,decimal_places=2,default=0)
    descrição = models.TextField(null=True, blank=True)

    
    data_atualização_registro = models.DateTimeField(auto_now=True)
    data_criação_registro = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-data_atualização_registro','-data_criação_registro']
    def __str__(self):
        return self.nome

class tbl_Transação(models.Model):
    valor = models.DecimalField(max_digits=12,decimal_places=2,default=0)
    descrição = models.TextField(null=True, blank=True)
    data = models.DateField(default=datetime.date.today)
    
    #-----variação no inventário----------------- 
    #id Item
    item_inventário=models.ForeignKey(
        tbl_Inventario,
        on_delete=models.SET_NULL,
        null=True, blank=True)
    variação_item_inventário = models.IntegerField(null=True, blank=True)
    
    #Associação a um serviço
    #id tbl_Serviço_prestador_serviço
    serviço = models.ForeignKey(
        tbl_Serviço,
        on_delete=models.SET_NULL,
        null=True, blank=True
    )

    data_atualização_registro = models.DateTimeField(auto_now=True)
    data_criação_registro = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.descrição


