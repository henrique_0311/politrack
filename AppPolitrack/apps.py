from django.apps import AppConfig


class ApppolitrackConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'AppPolitrack'
