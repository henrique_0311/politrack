from .models import *


def contexto_total(request):
    try:
        if request.user.tipo == "Cliente":
            Serviços_em_execução_cliente = tbl_Serviço.objects.filter(
                                        status = "Em execução",
                                        cliente = request.user
                                    )
            Solicitações_de_orçamento_cliente = tbl_Serviço.objects.filter(
                                        status = "Em processamento",
                                        cliente = request.user
                                    )

            orçamentos = tbl_Serviço.objects.filter(
                                    status = "Solicitação Avaliada",
                                    cliente = request.user
                                )
        else:
            Serviços_em_execução_cliente = None
            Solicitações_de_orçamento_cliente = None
            orçamentos = None

    except:
        Serviços_em_execução_cliente = None
        Solicitações_de_orçamento_cliente = None
        orçamentos = None

    

    context = {
        'Solicitações_de_orçamento': tbl_Serviço.objects.filter(status = "Em processamento"),
        'orçamentos': orçamentos,
        'Serviços_em_execução':tbl_Serviço.objects.filter(
                                    status = "Em execução"
                                ),
        'Serviços_em_execução_cliente': Serviços_em_execução_cliente,
        'Solicitações_de_orçamento_cliente':Solicitações_de_orçamento_cliente,
        'inventario': tbl_Inventario.objects.all()
        
        
    }
    return context