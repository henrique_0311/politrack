from django.forms import ModelForm
from .models import *
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.forms.widgets import HiddenInput

class Form_TipoServiço(ModelForm):
    class Meta:
        model = tbl_TipoServiço
        fields = '__all__'

class Form_Cria_Cliente(UserCreationForm):
    password1 = forms.CharField(
        label=("Senha"),
        widget=forms.PasswordInput
    )
    password2 = forms.CharField(
        label=("Confirme a Senha"),
        widget=forms.PasswordInput,
        help_text= None
    )
    class Meta:

        model = Usuario
        fields = ['username','email','CPF_ou_CNPJ','tipo']
        labels = {
            "username"   : "Usuário * ",
            "CPF_ou_CNPJ": "CPF ou CNPJ * ",
        
        }
        help_texts = {
            'username': None,
        }
        widgets = {
            'tipo': HiddenInput(),
        }
        

class Form_Cria_PrestadorDeServiço(UserCreationForm):
    password1 = forms.CharField(
        label=("Senha"),
        widget=forms.PasswordInput
    )
    password2 = forms.CharField(
        label=("Confirme a Senha"),
        widget=forms.PasswordInput,
        help_text= None
    )
    class Meta:
        model = Usuario
        fields = ['username','email','CPF_ou_CNPJ','tipo']
        labels = {
            "username"   : "Usuário * ",
            "CPF_ou_CNPJ": "CPF ou CNPJ * ",
        
        }
        help_texts = {
            'username': None,
        }
        widgets = {
            'tipo': HiddenInput(),
        }


class Form_Inventario(ModelForm):
    class Meta:
        model = tbl_Inventario
        fields = ['nome','descrição']

class Form_Serviço_1(ModelForm):
    class Meta:
        model = tbl_Serviço
        fields = ['nome','descrição','tipo_serviço','data_limite','data_preferencial',
                    'cliente','status']
        widgets = {
            'cliente': HiddenInput(),
            'status': HiddenInput(),
        }
        
        labels = {
            "nome"   : "Nome * ",
            "descrição": "Descrição * ",
            "tipo_serviço" : "Tipo serviço *",
            "data_limite":"Data limite *"
        }

class Form_Serviço_2(ModelForm):
    class Meta:
        model = tbl_Serviço
        fields = ['data_prevista','data_validade_orçamento','valor',
                  'status']
        widgets = {
            'status': HiddenInput(),
        }



class Form_Serviço_PrestadorServiço(ModelForm):
    class Meta:
        model = tbl_Serviço_PrestadorServiço
        fields = '__all__'

class Form_Transação(ModelForm):
    class Meta:
        model = tbl_Transação
        fields = '__all__'

class Form_Lan1(ModelForm): #despesa Financeira
    class Meta:
        model = tbl_Transação
        fields = ['valor','descrição','data',
                    'serviço']

        widgets = {
            'serviço': HiddenInput(),
        }

class Form_Lan2(ModelForm): #despesa Financeira
    class Meta:
        model = tbl_Transação
        fields = ['valor','descrição','data',
                    'serviço']

        widgets = {
            'serviço': HiddenInput(),
        }

class Form_Est1(ModelForm): #Despesa (saida de estoque)
    class Meta:
        model = tbl_Transação
        fields = ['item_inventário','variação_item_inventário','descrição','data',
                    'serviço','valor']

        widgets = {
            'serviço': HiddenInput(),
            'valor': HiddenInput(),
        }

class Form_Est2(ModelForm):# Entrada de item no estoque
    class Meta:
        model = tbl_Transação
        fields = ['item_inventário','variação_item_inventário','valor','descrição','data',
                    'serviço']

        widgets = {
            'serviço': HiddenInput(),
        }

#inclusão_horas
class Form_inclusão_horas(ModelForm):
    class Meta:
        model = tbl_Serviço_PrestadorServiço
        fields = '__all__'
        widgets = {
            'serviço': HiddenInput(),
            'prestador_de_serviço': HiddenInput()
        }

        