from django.urls import path
from django.http import HttpResponse
from . import views, apps



app_name =  apps.ApppolitrackConfig.name
urlpatterns = [
    path(''                                             , views.home        , name = "home"),
    path('login/'                                       , views.loginPage   , name = "login"),
    path('logout/'                                      , views.logoutPage  , name = "logout"),
    #Se cadastrar
    path('Cadastro/Gerente/'                            , views.cdr0        , name = "cdr0"),
    path('Cadastro/PrestadorServiço/'                   , views.cdr1        , name = "cdr1"),
    path('Cadastro/Cliente/'                            , views.cdr3        , name = "cdr3"),
    #
    path('TipoDeServiço/Novo'                           , views.cdr2        , name = "cdr2"),
    path('Inventário/NovoTipoDeItem'                    , views.cdr4        , name = "cdr4"),
    path('Inventário/Existentes'            , views.exibe_Inventario        , name = "exibe_Inventario"),
    #
    path('SolicitaçõesOrçamento/Nova/'                  , views.orç1        , name = "orç1"),
    path('SolicitaçõesOrçamento/Existentes/'            , views.orç2_1      , name = "orç2_1"),
    path('SolicitaçõesOrçamento/<int:pk>/Responder'     , views.orç2_2      , name = "orç2_2"),
    path('Orçamentos_recebidos/'                        , views.os1_1       , name = "os1_1"),
    path('Serviços_em_andamento/'                       , views.exibe_serv_em_andamento, name = "exibe_serv_em_andamento"),

    path('ServiçosEmAndamento/<int:pk>/NovaDespesa/'    , views.lan1        , name = "lan1"),
    path('ServiçosEmAndamento/<int:pk>/NovaReceita/'    , views.lan2        , name = "lan2"),
    path('ItemInventario/<int:pk>/Saida'                , views.est1        , name = "est1"),
    path('ItemInventario/Entrada/'              , views.est2        , name = "est2"),
    path('ServiçosEmAndamento/<int:pk>/IncluirHoras/',views.inclusão_horas,name='inclusão_horas'),


    path('Gera_ordem_de_serviço/<int:pk>/<str:ação>/', views.os1_2, name = "os1_2"),
   

]


