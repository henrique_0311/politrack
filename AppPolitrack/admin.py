from django.contrib import admin

# Register your models here.
from .models import *

lista_modelos_importados = [
    Usuario,
    tbl_TipoServiço,
    tbl_Inventario,
    tbl_Serviço,
    tbl_Serviço_PrestadorServiço,
    tbl_Transação,
]

for  modelo in lista_modelos_importados:
    admin.site.register(modelo)